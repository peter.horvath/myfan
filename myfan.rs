use std::fs;
use std::process::exit;
use std::thread;
use std::time::Duration;

fn die(msg: &str) {
  println!("{}", msg);
  exit(-1);
}

fn iv(i_min: f32, i_max: f32, o_min: f32, o_max: f32, i: f32) -> f32 {
  if i < i_min {
    return o_min;
  } else if i < i_max {
    return (i-i_min)/(i_max-i_min)*(o_max-o_min)+o_min;
  } else {
    return o_max;
  }
}

fn file_exists(path: String) -> bool {
  return fs::metadata(path).is_ok();
}

fn u32_to_file(n: u32, path: String) {
  let n_str = n.to_string();
  let result = fs::write(path, n_str);
  if result.is_err() {
      die ("can not append {n_str} to {path}");
  }
}

fn u32_from_file(path: String) -> u32 {
  let temp_result = fs::read_to_string(path);
  if temp_result.is_err() {
    die("can not read {path}");
  }
  let temp_str: String = temp_result.unwrap();
  let temp_str_trim: String = temp_str.trim().to_string();
  return temp_str_trim.parse().unwrap();
}

fn main() {
  const TEMPERATURE_FILES: [&str; 5] = [
    "/sys/devices/platform/coretemp.0/hwmon/hwmon2/temp1_input",
    "/sys/devices/platform/coretemp.0/hwmon/hwmon2/temp2_input",
    "/sys/devices/platform/coretemp.0/hwmon/hwmon2/temp3_input",
    "/sys/devices/platform/coretemp.0/hwmon/hwmon2/temp4_input",
    "/sys/devices/platform/coretemp.0/hwmon/hwmon2/temp5_input"];

  const PWM_FILES: [&str; 1] = [
    "/sys/devices/platform/asus-nb-wmi/hwmon/hwmon3/pwm1"
  ];

  const FREQ_FILES: [&str; 8] = [
    "/sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq",
    "/sys/devices/system/cpu/cpufreq/policy1/scaling_max_freq",
    "/sys/devices/system/cpu/cpufreq/policy2/scaling_max_freq",
    "/sys/devices/system/cpu/cpufreq/policy3/scaling_max_freq",
    "/sys/devices/system/cpu/cpufreq/policy4/scaling_max_freq",
    "/sys/devices/system/cpu/cpufreq/policy5/scaling_max_freq",
    "/sys/devices/system/cpu/cpufreq/policy6/scaling_max_freq",
    "/sys/devices/system/cpu/cpufreq/policy7/scaling_max_freq"
  ];

  const CPU_ONLINE_FILES: [&str; 8] = [
    "/sys/devices/system/cpu/cpu0/online",
    "/sys/devices/system/cpu/cpu1/online",
    "/sys/devices/system/cpu/cpu2/online",
    "/sys/devices/system/cpu/cpu3/online",
    "/sys/devices/system/cpu/cpu4/online",
    "/sys/devices/system/cpu/cpu5/online",
    "/sys/devices/system/cpu/cpu6/online",
    "/sys/devices/system/cpu/cpu7/online"
  ];

  const LOADAVG_FILE: &str = "/proc/loadavg";

  // const TARGET_TEMP: f32 = 85.0;
  const TARGET_TEMP: f32 = 75.0;
  const TIC_S: f32 = 0.1;
  const D_T_N: usize = 6;
  const C_D: f32 = 3.33/128.0;
  const C_C: f32 = 1.67/128.0;
  const C_I: f32 = 2.0/128.0;
  // const I_ERR_INFL: f32 = 0.9;
  const FREQ_MIN: u32 = 800000;
  const FREQ_MAX: u32 = 2900000;
  const PWM_MIN: u32 = 0;
  const PWM_MAX: u32 = 255;
  const CPU_MIN: u32 = 0;
  const CPU_MAX: u32 = 7;
  const CPU_ORDER: [u32; (CPU_MAX - CPU_MIN) as usize] = [4, 2, 6, 1, 5, 3, 7];

  let mut current_temp = TARGET_TEMP;
  let mut t_hist = [TARGET_TEMP; D_T_N];
  let mut t_est: f32;
  let mut i_err = 0.0;
  let mut freq: u32 = FREQ_MIN;
  let mut pwm: u32 = PWM_MAX;
  let mut cpu_online_n: u32 = CPU_MAX;
  let mut prev_freq: u32;
  let mut prev_pwm: u32;
  let mut prev_cpu_online_n: u32;
  let mut cpu_online_fst = CPU_MIN;
  let mut cpu_online_lst = CPU_MAX - 1;
  let mut cpu_mask: u32 = 0xff;

  for online_file in CPU_ONLINE_FILES.iter() {
    if file_exists(online_file.to_string()) {
      u32_to_file(1, online_file.to_string());
    }
  }

  loop {
    let mut fst: bool = true;
    for temp_file in TEMPERATURE_FILES.iter() {
      if ! file_exists(temp_file.to_string()) {
        continue;
      }
      let temp: f32 = u32_from_file(temp_file.to_string()) as f32;
      if fst || temp > current_temp {
        fst = false;
        current_temp = temp as f32 / 1000.0;
      }
    }
    if fst {
      die("no valid temp path found");
    }
    thread::sleep(Duration::from_millis((TIC_S*1000.0) as u64));
    let mut t_tmpsum1: f32 = 0.0;
    let mut t_tmpsum0: f32 = 0.0;
    for n in 0..(D_T_N-1) {
      t_tmpsum1 += t_hist[n] * n as f32/ D_T_N as f32/ D_T_N as f32;
      t_tmpsum0 += t_hist[n] / D_T_N as f32;
      t_hist[n] = t_hist[n+1];
    }
    t_hist[D_T_N-1]=current_temp;
    t_tmpsum1 += current_temp / D_T_N as f32;
    t_tmpsum0 += current_temp / D_T_N as f32;
    let app_a = 12.0 * t_tmpsum1 - 6.0 * t_tmpsum0;
    let app_b = 6.0 * t_tmpsum1 - 4.0 * t_tmpsum0;
    t_est = app_a + app_b;
    let d_t: f32 = (current_temp - t_hist[0])/(TIC_S * (D_T_N as f32));
    let current_err = current_temp - TARGET_TEMP;
    let d_err = d_t;
    println!("current_temp: {current_temp}, d_T: {d_t}, est. temp: {t_est} ({app_a}x+{app_b}, M1: {t_tmpsum1}, M0: {t_tmpsum0})");
    println!("d_err: {d_err}, current_err: {current_err}, i_err: {i_err}");
    let ctrl = C_D * d_err + C_C * current_err + C_I * i_err;

    println!("ctrl={ctrl}");

    prev_freq = freq;
    prev_pwm = pwm;
    prev_cpu_online_n = cpu_online_n;

    /*
    freq = iv(-0.5, 0.0, FREQ_MAX as f32, FREQ_MIN as f32, ctrl) as u32;
    cpu_online_n = iv(0.0, 0.5, CPU_MAX as f32, CPU_MIN as f32, ctrl) as u32;
    pwm = iv(-1.0, -0.5, 0.0, 64.0, ctrl) as u32 + iv(0.5, 1.0, 64.0, 255.0, ctrl) as u32 - 64;
    */

    freq = iv(0.0, 0.5, FREQ_MAX as f32, FREQ_MIN as f32, ctrl) as u32;
    cpu_online_n = iv(0.5, 1.0, CPU_MAX as f32, CPU_MIN as f32, ctrl) as u32;
    pwm = iv(-1.0, 0.0, 0.0, 255.0, ctrl) as u32;

    if ! ((ctrl < -1.0 && i_err < 0.0) || (ctrl > 1.0 && i_err > 0.0)) {
      i_err += current_err * TIC_S;
    }

    if freq != prev_freq {
      for n in CPU_MIN..=CPU_MAX {
    if file_exists(FREQ_FILES[n as usize].to_string()) && ((cpu_mask & (1<<n)) > 0) {
          u32_to_file(freq, FREQ_FILES[n as usize].to_string());
        }
      }
    }

    if pwm != prev_pwm {
      for pwm_file in PWM_FILES.iter() {
        u32_to_file(pwm, pwm_file.to_string());
      }
    }
    
    if cpu_online_n != prev_cpu_online_n {
      if cpu_online_n < prev_cpu_online_n {
        for _n in 0..(prev_cpu_online_n - cpu_online_n) {
          let cpu: u32 = CPU_ORDER[cpu_online_fst as usize];
          cpu_online_fst+=1;
          if cpu_online_fst >= CPU_MAX - CPU_MIN {
            cpu_online_fst = 0;
          }
          u32_to_file(0, CPU_ONLINE_FILES[cpu as usize].to_string());
          cpu_mask &= !(1<<cpu);
        }
      } else {
        for _n in 0..(cpu_online_n - prev_cpu_online_n) {
          cpu_online_lst+=1;
          if cpu_online_lst >= CPU_MAX - CPU_MIN {
            cpu_online_lst = 0;
          }
          let cpu: u32 = CPU_ORDER[cpu_online_lst as usize];
          u32_to_file(1, CPU_ONLINE_FILES[cpu as usize].to_string());
          cpu_mask |= 1<<cpu;
        }
      }
    }

    println!("pwm={pwm} freq={freq} cpu_mask={:#010b}", cpu_mask);
  }
}
