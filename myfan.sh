#!/bin/bash
PWM_MIN=0
PWM_MAX=255
PWM_TEMP_MIN=56000
PWM_TEMP_MAX=66000
PWM_DEC_MAX=16
PWM_INC_MAX=128

FREQ_MIN=800000
FREQ_MAX=2900000
FREQ_TEMP_MIN=46000
FREQ_TEMP_MAX=56000
FREQ_DEC_MAX=1000000
FREQ_INC_MAX=100000

DT=0.1
TS_FORMAT="%Y.%m.%d %H:%M:%S.%N"

function TempToPwm() {
  local temp="$1"
  if [ "$temp" -lt "${PWM_TEMP_MIN}" ]
  then
    echo "${PWM_MIN}"
  elif [ "$temp" -lt "${PWM_TEMP_MAX}" ]
  then
    echo "$[PWM_MIN+(temp-PWM_TEMP_MIN)*(PWM_MAX-PWM_MIN)/(PWM_TEMP_MAX-PWM_TEMP_MIN)]"
  else
    echo "${PWM_MAX}"
  fi
}

function TempToFreq() {
  local temp="$1"
  if [ "$temp" -lt "${FREQ_TEMP_MIN}" ]
  then
    echo "${FREQ_MAX}"
  elif [ "$temp" -lt "${FREQ_TEMP_MAX}" ]
  then
    echo "$[FREQ_MAX-(temp-FREQ_TEMP_MIN)*(FREQ_MAX-FREQ_MIN)/(FREQ_TEMP_MAX-FREQ_TEMP_MIN)]"
  else
    echo "${FREQ_MIN}"
  fi
}

function getTemp() {
  local curTemp
  local maxTemp=0
  for i in /sys/devices/platform/coretemp.0/hwmon/hwmon2/temp*_input
  do
    curTemp=$(<$i)
    [ "$curTemp" -gt "$maxTemp" ] && maxTemp="$curTemp"
  done
  echo "$maxTemp"
}

function setPwm() {
  local curPwm="$1"
  echo "$curPwm" >/sys/devices/platform/asus-nb-wmi/hwmon/hwmon4/pwm1
}

function setFreq() {
  local i
  local curFreq="$1"
  for i in /sys/devices/system/cpu/cpufreq/policy*/scaling_max_freq
  do
    echo "$curFreq" >"$i"
  done
}

temp="$(getTemp)"

prevPwm="${PWM_MAX}"
curPwm="${PWM_MAX}"
setPwm "${PWM_MAX}"

prevFreq="${FREQ_MIN}"
curFreq="${FREQ_MIN}"
setFreq "${FREQ_MIN}"

while true
do
  nextTemp="$(getTemp)"
  temp=$[(9*temp+nextTemp)/10]

  result="$(date +"${TS_FORMAT}") Temperature: ${temp}."
  nl="\r"

  curPwm=$(TempToPwm $temp)
  if [ "$curPwm" -lt "$[prevPwm-PWM_DEC_MAX]" ]
  then
    curPwm="$[prevPwm-PWM_DEC_MAX]"
  elif [ "$curPwm" -gt "$[prevPwm+PWM_INC_MAX]" ]
  then
    curPwm="$[prevPwm+PWM_INC_MAX]"
  fi
  if [ "$curPwm" -ne "$prevPwm" ]
  then
    result="${result} Pwm changed from ${prevPwm} to ${curPwm}."
    nl="\n"
    prevPwm="$curPwm"
    setPwm "$curPwm"
  fi

  curFreq=$(TempToFreq $temp)
  if [ "$curFreq" -lt "$[prevFreq-FREQ_DEC_MAX]" ]
  then
    curFreq="$[prevFreq-FREQ_DEC_MAX]"
  elif [ "$curFreq" -gt "$[prevFreq+FREQ_INC_MAX]" ]
  then
    curFreq="$[prevFreq+FREQ_INC_MAX]"
  fi
  if [ "$curFreq" -ne "$prevFreq" ]
  then
    result="${result} Freq changed from ${prevFreq} to ${curFreq}."
    nl="\n"
    prevFreq="$curFreq"
    setFreq "$curFreq"
  fi

  echo -ne "\r${result}${nl}"

  sleep "$DT"
done
